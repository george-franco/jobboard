import React from "react";
import { useNavigate } from "react-router-dom"


function MainPage() {
    const navigate = useNavigate()


    async function handleSubmit() {
        navigate("/")
    }

    return (
        <div>
            <form onSubmit={() => handleSubmit()}>
                <h1>My Webpage</h1>
                <button type="submit">Create a new portfolio</button>
            </form>
        </div>
    )
}

export default MainPage;