import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import SignupForm from './accounts/signup';
import LoginForm from './accounts/login';
import Nav from './Nav';
import MainPage from './MainPage';
import AccountSelection from './accounts/AccountSelection';
import CreateJobSeeker from './accounts/CreateJobseeker';
import './App.css';
import axios from 'axios';
import { useState } from 'react';


const App = () => {
  const [token, setToken] = useState(localStorage.getItem('token') || '');
  const handleLogout = () => {
      setToken('');
      localStorage.removeItem('token');
      axios.post('http://localhost:8000/api/logout/')
          .then(res => console.log(res.data.message))
          .catch(err => console.log(err));
  };

  return (
    <Router>
    <Nav token={token} handleLogout={handleLogout} />
      <Routes>
        <Route path='' element={<MainPage />} />
        <Route path='/login' element={<LoginForm />} />
        <Route path='/signup' element={<SignupForm />} />
        <Route path='/account-type' element={<AccountSelection />} />
        <Route path='/create-job-seeker' element={<CreateJobSeeker />} />
      </Routes>
    </Router>
  );
};

export default App;
