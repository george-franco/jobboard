import React from "react";
import { NavLink } from "react-router-dom";

const Nav = () => {
  const token = localStorage.getItem("token");
  console.log("TOKEN NAV", token)

  return (
    <nav>
      <NavLink className="nav-link" to="/">Home</NavLink>
      {token ?  (
      <NavLink className="nav-link" onClick={() => localStorage.clear('token')}>
        Logout
      </NavLink> 
        )  : (
          <>
            <NavLink className="nav-link" to="/login">Login</NavLink>
            <NavLink className="nav-link" to="/signup">Sign up</NavLink>
          </>
      )}
    </nav>
  );
};

export default Nav;