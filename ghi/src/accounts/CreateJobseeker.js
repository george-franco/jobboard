import { useNavigate } from "react-router-dom";
import { useState } from "react";
import axios from 'axios';


const CreateJobSeeker = () => {
    const [title, setJobTitle] = useState('')
    const [resume, setResume] = useState('')
    const [city, setCity] = useState('')
    const [state, setState] = useState('')
    const [linkedin, setLinkedIn] = useState('')


    const handleSubmit = async (e) => {
        e.preventDefault();

        try {
            const res = await axios.post('http://localhost:8000/api/jobseekers/', { title, resume, city, state, linkedin });
            console.log(res.data.message);
        } catch (error) {
            console.log(error);
        }
    };


    return (
        <>
        <h1>New job seeker</h1>
        <form onSubmit={handleSubmit}  enctype="multipart/form-data">
            <label>
                Your Job Title:
                <input type="text" value={title} onChange={(e) => setJobTitle(e.target.value)} />
            </label><br></br>
            <label>
                Your Resume:
                <input type="file" onChange={(e) => setResume(e.target.files[0])} />
            </label><br></br>
            <label>
                Your City:
                <input type="text" value={city} onChange={(e) => setCity(e.target.value)} />
            </label><br></br>
            <label>
                Your State:
                <input type="text" value={state} onChange={(e) => setState(e.target.value)} />
            </label><br></br>
            <label>
                Your LinkedIn:
                <input type="text" value={linkedin} onChange={(e) => setLinkedIn(e.target.value)} />
            </label>
            <button type="submit">Sign Up</button>
        </form>
        </>
    )
}


export default CreateJobSeeker;