import { useNavigate } from "react-router-dom";


const AccountSelection = () => {
    const navigate = useNavigate();

    const handleSubmit = (path) => {
      navigate(path);
    };

    return (
        <form>
            <h1>What type of user are you?</h1>
            <button type='submit' value='createJobseeker' onClick={() => handleSubmit("/create-job-seeker")}>Jobseeker</button>
            <button type='submit' value='createBusiness' onClick={() => handleSubmit("/createBusiness")}>Employer</button>
        </form>
    )
}


export default AccountSelection;