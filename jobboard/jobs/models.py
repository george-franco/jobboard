from django.db import models
from accounts.models import Businesses, Jobseekers

# Create your models here.
class Job(models.Model):
    title = models.CharField(max_length=100)
    description = models.TextField()
    salary = models.BigIntegerField()
    business = models.ForeignKey(Businesses, on_delete=models.PROTECT)

    def __str__(self):
        return self.title, self.business


class Application(models.Model):
    job = models.ForeignKey(Job, on_delete=models.PROTECT)
    applicant = models.ForeignKey(Jobseekers, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)