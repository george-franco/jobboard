from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from .models import Businesses, Job
# Create your views here.


def create_job_posting(request):
    if request.method == "POST":
        title = request.POST.get('title')
        description = request.POST.get('description')
        salary = request.POST.get('salary')
        business_id = request.POST.get('business')
        business = Businesses.objects.get(id=business_id)
        job = Job(title=title, description=description, salary=salary, business=business)
        job.save()
        return redirect("/")
    businesses = Businesses.objects.all()
    context = {'businesses': businesses}
    return redirect(request, '/', context)
