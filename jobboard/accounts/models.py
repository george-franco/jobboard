from django.db import models
from django.contrib.auth.models import User
from django.conf import settings
# Create your models here.


# class User(models.Model):
#     username = models.CharField(max_length=50, unique=True)
#     email = models.EmailField(unique=True)
#     password = models.CharField(max_length=50)

#     def __str__(self):
#         return self.username


class Jobseekers(models.Model):
    user = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    job_title = models.CharField(max_length=50, default=None, blank=True)
    resume = models.FileField(upload_to='resumes/')
    city = models.CharField(max_length=30, default=None, blank=True)
    state = models.CharField(max_length=30, default=None, blank=True)
    linkedin = models.URLField(max_length=1000, default=None, blank=True)

    def __str__(self):
        return f"{self.user.username} - {self.job_title}"


class Businesses(models.Model):
    user = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    name = models.CharField(max_length=30, default=None, blank=True)
    city = models.CharField(max_length=30, default=None, blank=True)
    state = models.CharField(max_length=30, default=None, blank=True)
    url = models.URLField(max_length=1000, default=None, blank=True)

    def __str__(self):
        return self.name or ''