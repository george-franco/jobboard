from django.urls import path
from .views import register, login, logout, create_business, create_jobseeker

urlpatterns = [
    path('register/', register, name='register'),
    path('login/', login, name='login'),
    path('logout/', logout, name='logout'),
    path('jobseekers/', create_jobseeker, name='jobseeker'),
    path('business/', create_business, name='business'),
]