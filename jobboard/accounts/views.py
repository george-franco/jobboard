from django.contrib.auth import authenticate
from rest_framework import status
from rest_framework.response import Response
from rest_framework.decorators import api_view
from rest_framework_simplejwt.tokens import RefreshToken
from django.contrib.auth.models import User
from django.contrib.auth import authenticate
from django.contrib.auth.decorators import login_required
from .models import Jobseekers, Businesses
from django.shortcuts import redirect
# Create your views here.


@api_view(['POST'])
def register(request):
    username = request.data.get('username')
    email = request.data.get('email')
    password = request.data.get('password')

    if not username or not email or not password:
        return Response({'error': 'Please provide all required fields'}, status=status.HTTP_400_BAD_REQUEST)

    user, created = User.objects.get_or_create(username=username, email=email, password=password)

    if not created:
        return Response({'error': 'User already exists'}, status=status.HTTP_400_BAD_REQUEST)

    return Response({'message': 'User created successfully'}, status=status.HTTP_201_CREATED)


@api_view(['POST'])
def login(request):
    username = request.data.get('username')
    password = request.data.get('password')

    if not username or not password:
        return Response({'error': 'Please provide both username and password'}, status=status.HTTP_400_BAD_REQUEST)

    user = authenticate(username=username, password=password)

    if not user:
        return Response({'error': 'Invalid credentials'}, status=status.HTTP_401_UNAUTHORIZED)
    refresh = RefreshToken.for_user(user)
    return Response({
        'refresh': str(refresh),
        'access': str(refresh.access_token),
    }, status=status.HTTP_200_OK)


@api_view(['POST'])
def logout(request):
    return Response({'message': 'Logout successful'}, status=status.HTTP_200_OK)


@login_required
def create_jobseeker(request):
    user = request.user
    if request.method == 'POST':
        job_title = request.POST['job_title']
        city = request.POST['city']
        state = request.POST['state']
        linkedin = request.URL['linkedin']
        resume = request.FILES['resume']
        jobseeker = Jobseekers.objects.create(user=user, job_title=job_title, city=city, state=state, linkedin=linkedin, resume=resume)
        jobseeker.save()
    return redirect('/')


@login_required
def create_business(request):
    user = request.user
    if request.method == 'POST':
        name = request.POST['name']
        city = request.POST['city']
        state = request.POST['state']
        url = request.URL['url']
        business = Businesses.objects.create(user=user, name=name, city=city, state=state, url=url)
        business.save()
    return redirect('/')
